# import of Textblob library for preprocessing
# http://textblob.readthedocs.io/en/dev/_modules/textblob/tokenizers.html
# This is the PROCESSING FILE which takes Tweets and based on the multi regex search
# outputs and and saves the output for viweing.
import json
import re
from itertools import chain

import nltk.tokenize
from nltk.tokenize import sent_tokenize
from textblob import TextBlob
from textblob.base import BaseTokenizer
from textblob.tokenizers import WordTokenizer


# Tokenization from TextBlob class

class SentenceTokenizer(BaseTokenizer):
    """NLTK's sentence tokenizer (currently PunkSentenceTokenizer).
        Uses an unsupervised algorithm to build a model for abbreviation words,
        collocations, and words that start sentences,
        then uses that to find sentence boundaries.
        """

    def tokenize(self, text):
        return nltk.tokenize.sent_tokenize(text)  # Return a list of sentences


# sent_tokenize tokenizes sentences.
sent_tokenize = SentenceTokenizer().itokenize

_word_tokenizer = WordTokenizer()  # Singleton word tokenizer


def word_tokenize(text, include_punc=True, *args, **kwargs):
    # Tokenizing text into words.
    words = chain.from_iterable(
        _word_tokenizer.itokenize(sentence, include_punc=include_punc,
                                  *args, **kwargs)
        for sentence in sent_tokenize(text))

    return words


# to count positive and negative tweets.
pos_count = 0
pos_correct = 0

neg_count = 0
neg_correct = 0

## Perth tweet count - 500000 tweets
##
# TweetPerth3 -16000000 tweets
# Perth4 count - 75998 tweets
# tweets.json had measles tweets - saved into works.json, work1 has ebola
# tweets geocoded2 has json issues - downloading had a problem

final_tweetData = []  # to store the processed tweet contents in a list

tweet_file = 'tweets2.json'
with open(tweet_file, 'r', encoding='utf-8-sig') as input_file:
    count = 0
    for line in input_file:
        tweet = json.loads(line)
        count += 1
        line = line.strip()

        tokensised_text = sent_tokenize(tweet['doc']['text'])
        tweet_text = tweet['doc']['text']  # searching in this tweet text
        # print("test: ", tweet)

        if re.search(r'malaria'
                     r'', tweet_text):
            tweet_id = tweet["id"]

            tweet_loc = tweet["doc"]["location"]
            tweet_date = tweet["doc"]["created_at"]

            # tweet_date = tweet["doc"]["user"]["created_at"] # for other tweet file due to different format

            # tweet_geo = str(tweet["value"]["geometry"]["coordinates"]) # for older tweet file.

            processed_text = ' '.join(
                tokensised_text)  # input is only text with no hashtag or @, output is not affected
            print(processed_text)

            analysis = TextBlob(processed_text)  # analysis is a class which stores  processed text

            final_tweetData.append(
                tweet_id + ',' + processed_text + ',   ' + tweet_loc + ' ,' + tweet_date + ' ,' + str(
                    analysis.sentiment_assessments))
            # print(tweet_loc)
            # print(tweet_id + ', ' + processed_text + ' ,' + tweet_loc + ' ,' " {}".format(analysis.sentiment_assessments))
            final_tweetData.append(''.join(str(analysis.sentiment_assessments)))

    out_tweet = '\n'.join(final_tweetData)  # tuple to a list
    print(out_tweet)

# saving the output to a desired file using UTF-8-SIG TWITTER ENCODING
with open("work10.json", "w", encoding='utf-8-sig') as write_file:
    write_file.write(out_tweet)
    write_file.close()
