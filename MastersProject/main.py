# This file is used to download tweets from the CouchDB Server using requests library
# and save it as JSON on local disk

import json

import requests

tweet_count = 0  # to verify tweet numbers
with open('newTweetfile.json', "w") as write_file:  # this file name is dummy, please replace it
    count = 0
    for i in range(10000):
        #  #------adelaide tweets and mixed-------------
        # resp = requests.get(
        #     "http://45.113.232.90/couchdbro/twitter/_design/twitter/_view/summary?include_docs=true&reduce=false&skip=0&limit=1000",
        #     auth=('readonly', 'ween7ighai9gahR6'))

        # -----------------geocoded ones only----------------------------------------
        resp = requests.get(
            "http://45.113.232.90/couchdbro/twitter/_design/twitter/_view/geoindex?include_docs=true&reduce=false&skip=0&limit=100",
            auth=('readonly', 'ween7ighai9gahR6'))

        resp = resp.json()
        rows = resp['rows']
        for line in rows:
            count += 1
            print('tweet count - ', count)
            json.dump(line, write_file)
            write_file.write('\n')
