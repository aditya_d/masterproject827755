import json
import re

from textblob import TextBlob

pos_count = 0
pos_correct = 0

neg_count = 0
neg_correct = 0


def cleanTweet(tweet_text):
    return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t]) | (\w +:\ / \ / \S +)", " ", tweet_text).split())


sentiment_TWEETS = []  # storing this in a file
with open('tweets.json', 'r', encoding='utf-8') as openfile:
    for line in openfile:
        if re.search(r'flu', line):
            tweet_data = json.loads(line)
            line = line.strip()
            # print(tweet_data)

            tweet_text = tweet_data["doc"]["text"]
            tweet_id = tweet_data["id"]

            # tweet_date = data["doc"]["user"]["created_at"]

            tweet_loc = tweet_data["doc"]["location"]

            analysis = TextBlob(tweet_text)
            # print(analysis.sentiment_assessments)

            if analysis.sentiment.subjectivity <= -0.0:
                if analysis.sentiment.polarity <= 0:
                    neg_correct += 1
                neg_count += 1

            # print(neg_correct)
            # print(neg_count)

            if analysis.sentiment.subjectivity >= 0.0:
                if analysis.sentiment.polarity > 0 or analysis.sentiment.polarity < 1:
                    pos_correct += 1
                pos_count += 1

            # print(pos_correct)
            # print(pos_count)
            # print("Positive accuracy = {}% via {} samples ".format(pos_correct / pos_count * 100.0, pos_count))
            # print("Negative accuracy = {}% via {} samples".format(neg_correct / neg_count * 100.0, neg_count))

            print(tweet_id + ', ' + tweet_text + " {}".format(analysis.sentiment))
        sentiment_TWEETS.append(tweet_id + tweet_text + tweet_loc + analysis)

with open('COLDSENTI.json', 'w', encoding='utf-8-sig') as openfile:
    openfile.write()
